# Pate play test

Run `npm i` to install all of the dependencies.

## Development server

Run `npm run start:dev` for a dev server. Navigate to `http://localhost:8080/`. The app will automatically reload if you change any of the source files.
