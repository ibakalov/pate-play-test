export type chipValues = 1 | 5 | 10 | 50;

export type chipSizes = 'small' | 'medium' | 'large';
