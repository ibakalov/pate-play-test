import React, { ReactElement, useContext, useEffect, useState } from 'react';
import { ChipValueContext } from '@patePlay/contexts/chip-value.context';
import { ChipChoosenValuesContext } from '@patePlay/contexts/chip-choosen-value.context';
import Chip from '@patePlay/components/chip/chip.component';
import Error from '@patePlay/components/error/error.component';
import { toKebabCase } from '@patePlay/helpers/to-kebap-case.helper';
import { ErrorBoundary } from 'react-error-boundary';

import styles from './line-buttons.component.module.css';

const LineButtons = React.memo(function LineButtons({ name, lineType }: { name: string | number; lineType: string }): ReactElement {
  const [choosenValues, setChoosenValues] = useContext(ChipChoosenValuesContext);
  const [chipValue] = useContext(ChipValueContext);
  const [combineName, setCombineName] = useState<string>('');

  const [typeOfNumberButton, setTypeOfNumberButton] = useState<'odd' | 'even' | 'not-number'>('not-number');

  const setChoosen = (): void => {
    setChoosenValues({ name, chipValue, lineType });
  };

  useEffect(() => {
    const tempCombinedName = typeof name === 'number' ? name.toString() : lineType + name.toString();

    if (typeof name === 'number') {
      setTypeOfNumberButton(name % 2 === 1 ? 'odd' : 'even');
    }

    setCombineName(tempCombinedName);
  }, [name, choosenValues, lineType]);

  return (
    <ErrorBoundary fallback={<Error />}>
      <div className={`${styles['button-wrapper']} ${(styles[toKebabCase('button-' + name)] && styles[toKebabCase('button-' + name)]) || ''}`}>
        <button onClick={setChoosen} className={`${typeOfNumberButton !== 'not-number' ? styles['number-button'] + ' ' + styles[typeOfNumberButton] : ''}`}>
          {name}
        </button>
        {choosenValues && choosenValues[combineName] && (
          <div className={styles['chip']}>
            <Chip setChoosen={setChoosen} chipSizes="small" chipValue={choosenValues[combineName].amount} />
          </div>
        )}
      </div>
    </ErrorBoundary>
  );
});

export default LineButtons;
