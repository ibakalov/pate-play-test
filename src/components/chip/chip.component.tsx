import React, { ReactElement } from 'react';
import { ErrorBoundary } from 'react-error-boundary';
import chipSvg from '../../../public/chip.embeded.svg';
import Error from '@patePlay/components/error/error.component';
import { chipSizes, chipValues } from '@patePlay/types/chip-values.type';
import styles from './chip.module.css';

const Chip = React.memo(function Chip({
  setChoosen,
  chipValue,
  chipSizes,
}: {
  setChoosen?: () => void;
  chipValue: chipValues;
  chipSizes: chipSizes;
}): ReactElement {
  return (
    <ErrorBoundary fallback={<Error />}>
      <div className={`${styles['svg-wrapper']} ${setChoosen ? styles['click'] : ''}`} onClick={setChoosen}>
        <span className={`${styles['amount']} ${styles['amount-' + chipSizes]}`}>{chipValue}</span>
        <div
          className={`${styles['color-' + chipValue]} ${styles['svg-element']} ${styles['size-' + chipSizes]}`}
          dangerouslySetInnerHTML={{ __html: chipSvg }}
        ></div>
      </div>
    </ErrorBoundary>
  );
});

export default Chip;
