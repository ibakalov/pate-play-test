import React, { ReactElement } from 'react';

import styles from './win-modal.module.css';

const WinModal = ({ winModalText, closeWinModal }: { winModalText: string; closeWinModal: () => void }): ReactElement => {
  return (
    <div onClick={closeWinModal} className={styles['modal-wrapper']}>
      <div className={styles['modal-inner-wrapper']}>
        <button onClick={closeWinModal} className={styles['close']}>
          X
        </button>
        <div className={styles['modal-inner']}>{winModalText}</div>
      </div>
    </div>
  );
};

export default WinModal;
