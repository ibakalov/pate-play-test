import React, { ReactElement, useCallback, useContext, useState } from 'react';
import { ChipValueContext } from '@patePlay/contexts/chip-value.context';
import { CHIP_VALUES } from '@patePlay/configs/chip-values.config';
import { ChipChoosenValuesContext } from '@patePlay/contexts/chip-choosen-value.context';
import { getRandomNumber } from '@patePlay/helpers/generate-random-numbers.helper';
import { WINNERS_CONFIG } from '@patePlay/configs/winners.config';
import Chip from '@patePlay/components/chip/chip.component';
import WinModal from '@patePlay/components/win-modal/win-modal';
import { UserContext } from '@patePlay/contexts/user.context';

import styles from './roulette-actions.component.module.css';

const RouletteActions = (): ReactElement => {
  const [chipValue, setChipValue] = useContext(ChipValueContext);
  const [_, setAmount] = useContext(UserContext);
  const [choosenValues, setChoosenValues] = useContext(
    ChipChoosenValuesContext
  );
  const [previousValues, setPreviousValues] = useState<Array<number>>([]);
  const [hotValues, setHotValues] = useState<Array<number>>([]);
  const [winModalText, setWinModalText] = useState<string>('');

  const changeChipValue = useCallback(
    (type: 'plus' | 'minus'): void => {
      const index = CHIP_VALUES.indexOf(chipValue) + (type === 'plus' ? 1 : -1);

      setChipValue(CHIP_VALUES[index]);
    },
    [chipValue, setChipValue]
  );

  const closeWinModal = (): void => {
    setWinModalText('');
  };

  const spin = useCallback(() => {
    let winAmount = 0;
    const spin = getRandomNumber(0, 36);
    setPreviousValues((previousValue) => {
      const newValue = [...previousValue, spin];
      const getHotNumbers = newValue.reduce((prev, next) => {
        return {
          ...prev,
          [next]: prev[next] ? prev[next] + 1 : 1,
        };
      }, {} as Record<string, number>);

      if (getHotNumbers[spin] >= 3) {
        setHotValues((oldValue) => {
          return Array.from(new Set([...oldValue, spin]));
        });
      }

      if (choosenValues[spin.toString()])
        winAmount = choosenValues[spin.toString()].amount * 36;
      else {
        for (const choosenValueKey in choosenValues) {
          if (
            Object.prototype.hasOwnProperty.call(choosenValues, choosenValueKey)
          ) {
            if (WINNERS_CONFIG[choosenValueKey]) {
              const { values, winCoeficient } = WINNERS_CONFIG[choosenValueKey];

              if (values.indexOf(spin) > 0) {
                winAmount =
                  winAmount +
                  winCoeficient * choosenValues[choosenValueKey].amount;
              }
            }
          }
        }
      }

      setWinModalText(
        `Win value ${spin} - ${spin % 2 !== 0 ? 'black, odd' : 'red, even'} ${
          winAmount ? 'You win ' + winAmount : ''
        }`
      );

      const reducedAmount = Object.keys(choosenValues).reduce((prev, index) => {
        return prev + choosenValues[index].amount;
      }, 0);

      setAmount((previousAmount) => {
        setChoosenValues({});

        return previousAmount - reducedAmount + winAmount;
      });

      return newValue;
    });
  }, [choosenValues, setChoosenValues]);

  return (
    <>
      {winModalText && (
        <WinModal winModalText={winModalText} closeWinModal={closeWinModal} />
      )}
      <div className={styles['values-array']}>
        <div>
          <h2>Last five numbers</h2>
          {previousValues.length > 0 &&
            previousValues.slice(-5).map((previousValue) => {
              return (
                <span key={previousValue + getRandomNumber(0, 1000000)}>
                  {previousValue}
                </span>
              );
            })}
        </div>

        <div>
          <h2>Hot numbers</h2>
          {hotValues.length > 0 &&
            hotValues.slice(-5).map((previousValue) => {
              return (
                <span key={previousValue + getRandomNumber(0, 1000000)}>
                  {previousValue}
                </span>
              );
            })}
        </div>
      </div>

      <div className={styles['chip-wrapper']}>
        <button
          onClick={(): void => changeChipValue('minus')}
          className={styles.minus}
          disabled={!CHIP_VALUES[CHIP_VALUES.indexOf(chipValue) - 1]}
        >
          -
        </button>
        <Chip chipSizes='large' chipValue={chipValue} />
        <button
          onClick={(): void => changeChipValue('plus')}
          className={styles.plus}
          disabled={!CHIP_VALUES[CHIP_VALUES.indexOf(chipValue) + 1]}
        >
          +
        </button>

        <button onClick={spin} className={styles.spin}>
          Spin
        </button>
      </div>
    </>
  );
};

export default RouletteActions;
