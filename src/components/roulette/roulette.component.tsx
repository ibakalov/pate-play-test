import React, { ReactElement } from 'react';
import { LINES } from '@patePlay/configs/lines.config';
import { toKebabCase } from '@patePlay/helpers/to-kebap-case.helper';

import LineButtons from '@patePlay/components/line-buttons/line-buttons.component';
import RouletteActions from '@patePlay/components/roulette-actions/roulette-actions.component';
import User from '@patePlay/components/user/user.component';
import styles from './roulette.component.module.css';

function Roulette(): ReactElement {
  return (
    <div className={styles['main-wrapper']}>
      <div>
        <User />
        <div className={styles['inner-wrapper']}>
          <div className={styles['zero-section']}>
            <button>0</button>
          </div>
          <div className={styles.lines}>
            {Object.values(LINES).map((levelOne, lineType) => (
              <div
                key={lineType}
                className={`${styles['lines-section']} ${
                  styles[toKebabCase(Object.keys(LINES)[lineType])]
                }`}
              >
                {levelOne.map((lineLevelTwo: string | number) => (
                  <LineButtons
                    key={lineLevelTwo}
                    name={lineLevelTwo}
                    lineType={Object.keys(LINES)[lineType]}
                  />
                ))}
              </div>
            ))}
          </div>
        </div>
        <RouletteActions />
      </div>
    </div>
  );
}

export default Roulette;
