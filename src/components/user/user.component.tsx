import { ChipChoosenValuesContext } from '@patePlay/contexts/chip-choosen-value.context';
import { UserContext } from '@patePlay/contexts/user.context';
import React, { ReactElement, useContext, useEffect, useState } from 'react';
import styles from './User.component.module.css';

const User = (): ReactElement => {
  const [choosenValues] = useContext(ChipChoosenValuesContext);
  const [amount] = useContext(UserContext);
  const [tempAmount, setTempAmount] = useState(0);

  useEffect(() => {
    if (choosenValues && Object.keys(choosenValues).length > 0) {
      const reducedAmount = Object.keys(choosenValues).reduce((prev, index) => {
        return prev + choosenValues[index].amount;
      }, 0);

      setTempAmount(reducedAmount);
    } else {
      setTempAmount(0);
    }
  }, [choosenValues]);

  return (
    <div className={styles['user-wrapper']}>
      <h2>Amount: {amount - tempAmount}</h2>
    </div>
  );
};

export default User;
