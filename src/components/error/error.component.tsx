import React, { ReactElement } from 'react';

function Error(): ReactElement {
  return <h2>Error</h2>;
}

export default Error;
