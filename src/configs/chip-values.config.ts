import { chipValues } from 'src/types/chip-values.type';

export const CHIP_VALUES: Array<chipValues> = [1, 5, 10, 50];
