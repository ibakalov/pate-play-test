export const ALL_LINES = Array.from({ length: 36 }, (_, i) => i + 1);

export const LINES = {
  lineOne: [...ALL_LINES.filter((line) => line % 3 === 0), '2to1'],
  lineTwo: [...ALL_LINES.filter((line) => (line + 1) % 3 === 0), '2to1'],
  lineThree: [...ALL_LINES.filter((line) => (line + 2) % 3 === 0), '2to1'],
  oneFromEleventh: ['1st12', '2nd12', '3st12'],
  otherSpecial: ['1to18', 'EVEN', 'RED', 'BLACK', 'ODD', '19to36'],
};
