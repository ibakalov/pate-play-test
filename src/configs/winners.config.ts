import { ALL_LINES } from './lines.config';

export const WINNERS_CONFIG: Record<string, { winCoeficient: number; values: Array<number> }> = {
  lineOne2to1: { winCoeficient: 3, values: ALL_LINES.filter((line) => line % 3 === 0) },
  lineTwo2to1: { winCoeficient: 3, values: ALL_LINES.filter((line) => (line + 1) % 3 === 0) },
  lineThree2to1: { winCoeficient: 3, values: ALL_LINES.filter((line) => (line + 2) % 3 === 0) },
  oneFromEleventh1st12: { winCoeficient: 3, values: [...ALL_LINES].slice(0, 12) },
  oneFromEleventh2nd12: { winCoeficient: 3, values: [...ALL_LINES].slice(12, 24) },
  oneFromEleventh3st12: { winCoeficient: 3, values: [...ALL_LINES].slice(24, 36) },
  otherSpecial1to18: { winCoeficient: 2, values: [...ALL_LINES].slice(0, 18) },
  otherSpecial19to36: { winCoeficient: 2, values: [...ALL_LINES].slice(18, 36) },
  otherSpecialBLACK: { winCoeficient: 2, values: ALL_LINES.filter((line) => line % 2 > 0) },
  otherSpecialEVEN: { winCoeficient: 2, values: ALL_LINES.filter((line) => line % 2 === 0) },
  otherSpecialODD: { winCoeficient: 2, values: ALL_LINES.filter((line) => line % 2 > 0) },
  otherSpecialRED: { winCoeficient: 2, values: ALL_LINES.filter((line) => line % 2 === 0) },
};
