import React, { PropsWithChildren, useState } from 'react';
import { DEFAULT_AMOUNT } from '@patePlay/configs/default-amount.config';

const userContext: [number, (value: number) => void] = [
  DEFAULT_AMOUNT,
  (_: number): void => {
    return;
  },
];

export const UserContext =
  React.createContext<[number, (value: number) => void]>(userContext);

export const UserContextProvider: (props: PropsWithChildren) => JSX.Element = (
  props: PropsWithChildren
) => {
  const [state, setState] = useState<number>(DEFAULT_AMOUNT);

  return (
    <UserContext.Provider value={[state, setState]}>
      {props.children}
    </UserContext.Provider>
  );
};
