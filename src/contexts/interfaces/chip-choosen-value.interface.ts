import { chipValues } from '@patePlay/types/chip-values.type';

export interface choosenValuesInterface {
  amount: chipValues;
  name: string;
}

export type combinedViluesType = Record<string, choosenValuesInterface>;
