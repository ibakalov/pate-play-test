import { chipValues } from '@patePlay/types/chip-values.type';
import React, { PropsWithChildren, useState } from 'react';
import { combinedViluesType } from './interfaces/chip-choosen-value.interface';

const chipContextChoosenValues: [combinedViluesType, (value: Partial<{ name: number | string; chipValue: chipValues; lineType: string }>) => void] = [
  {},
  (): void => {
    return;
  },
];

export const ChipChoosenValuesContext =
  React.createContext<[combinedViluesType, (value: Partial<{ name: number | string; chipValue: chipValues; lineType: string }>) => void]>(
    chipContextChoosenValues
  );

export const ChipChoosenValuesContextProvider: (props: PropsWithChildren) => JSX.Element = (props: PropsWithChildren) => {
  const [state, setState] = useState<combinedViluesType>({});

  const setChoosenValues = ({ name, chipValue, lineType }: Partial<{ name: number | string; chipValue: chipValues; lineType: string }>): void => {
    setState((previousState) => {
      let values = { ...previousState };

      if (name && chipValue && lineType) {
        const tempCombinedName = typeof name === 'number' ? name.toString() : lineType + name.toString();

        if (tempCombinedName) {
          if (values[tempCombinedName]) {
            delete values[tempCombinedName];
          } else {
            values = { ...values, [tempCombinedName]: { amount: chipValue, name: name.toString() } };
          }
        }

        return values;
      } else return {};
    });
  };

  return <ChipChoosenValuesContext.Provider value={[state, setChoosenValues]}>{props.children}</ChipChoosenValuesContext.Provider>;
};
