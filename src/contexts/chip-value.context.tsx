import React, { PropsWithChildren, useState } from 'react';
import { chipValues } from '@patePlay/types/chip-values.type';

const chipContextValues: [chipValues, (value: chipValues) => void] = [
  1,
  (): void => {
    return;
  },
];

export const ChipValueContext = React.createContext<[chipValues, (value: chipValues) => void]>(chipContextValues);

export const ChipValueContextProvider: (props: PropsWithChildren) => JSX.Element = (props: PropsWithChildren) => {
  const [state, setState] = useState<chipValues>(1);

  return <ChipValueContext.Provider value={[state, setState]}>{props.children}</ChipValueContext.Provider>;
};
