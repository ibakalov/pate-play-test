export function toKebabCase(value: string): string {
  if (!value) return value;

  const valueToBerReturned = value
    .replace(/([a-z])([A-Z])/g, '$1-$2')
    .replace(/\s+/g, '-')
    .toLowerCase();

  return valueToBerReturned;
}
