import React, { ReactElement } from 'react';
import Roulette from './components/roulette/roulette.component';
import { ChipChoosenValuesContextProvider } from './contexts/chip-choosen-value.context';
import { ChipValueContextProvider } from './contexts/chip-value.context';
import { UserContextProvider } from './contexts/user.context';

function App(): ReactElement {
  return (
    <div className="App">
      <ChipValueContextProvider>
        <ChipChoosenValuesContextProvider>
          <UserContextProvider>
            <Roulette />
          </UserContextProvider>
        </ChipChoosenValuesContextProvider>
      </ChipValueContextProvider>
    </div>
  );
}

export default App;
